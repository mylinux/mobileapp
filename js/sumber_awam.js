var urlnegeri = "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/Demarcation/MapServer/0";
var urldaerah = "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/Demarcation/MapServer/1";
var urlmukim = "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/Demarcation/MapServer/2";
var urlkampung = "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/PDDM/MapServer/25";

var map, view;
var array_groupLayer = [];

let editorExpand_sork, layerKgInduk_sork, layerKgRangkaian_sork, layerEdit_sork;
let editorExpand_sort, layerKgInduk_sort, layerKgRangkaian_sort, layerEdit_sort;

const kemaskiniNamaBaruKemudahan = {
    name: "nama",
    label: "Nama Baru Kemudahan",
    // description: "Contoh: Balairaya Kampung Banting",
    // hint: "Sekolah ...",
    required: true,
    type: "text", //"number"|"text"|"date"|"unsupported"
    editable: true //Enable/disabled input
}

const kemaskiniCatatan = {
    name: "catatan",
    label: "Maklumat Lain (Jika Ada)",
    editorType: "text-area" //text-area,text-box
}
const inputNamaBaruKemudahan = {
    name: "nama",
    label: "Nama Baru Kemudahan",
    // description: "Contoh: Balairaya Kampung Banting",
    // hint: "Sekolah ...",
    required: true,
    type: "text", //"number"|"text"|"date"|"unsupported"
    editable: true //Enable/disabled input
}

const inputCatatan = {
    name: "catatan",
    label: "Maklumat Lain (Jika Ada)",
    editorType: "text-area" //text-area,text-box
}

var arrayJenisKemudahan_Kemaskini = {
    "Kemudahan Ekonomi": {
        "namaEkonomi": ["", "Bengkel/Kilang", "Chalet/Homestay", "Perkhidmatan Bank", "Pusat Penjaja"]
        // ,"Kedai","Gerai","Perusahaann IKS"
    },
    "Kemudahan Keagamaan": {
        "namaEkonomi": ["", "Perkuburan", "Rumah Ibadat"]
    },
    "Kemudahan Keselamatan": {
        "namaEkonomi": ["", "Bomba", "Polis", "Tapak Kubu Monumen"]
    },
    "Kemudahan Kesihatan": {
        "namaEkonomi": ["", "Kemudahan Kesihatan"]
    },
    "Kemudahan Pendidikan": {
        "namaEkonomi": ["", "Institusi Pendidikan", "Perpustakaan Desa", "Pusat ICT"]
    },
    "Kemudahan Pengangkutan": {
        "namaEkonomi": ["", "Kemudahan Pengangkutan Awam"]
    },
    "Kemudahan Pentadbiran Masyarakat": {
        "namaEkonomi": ["", "Kemudahan Pentadbiran & Masyarakat"]
    },
    "Kemudahan Rekreasi": {
        "namaEkonomi": ["", "Kemudahan Rekreasi", "Kemudahan Sukan", "Tanah Lapang"]
    }
}

var arrayJenisKemudahan = {
    "Kemudahan Ekonomi": {
        "namaEkonomi": ["", "Bengkel/Kilang", "Chalet/Homestay", "Perkhidmatan Bank", "Pusat Penjaja"]
        // ,"Kedai","Gerai","Perusahaann IKS"
    },
    "Kemudahan Keagamaan": {
        "namaEkonomi": ["", "Perkuburan", "Rumah Ibadat"]
    },
    "Kemudahan Keselamatan": {
        "namaEkonomi": ["", "Bomba", "Polis", "Tapak Kubu Monumen"]
    },
    "Kemudahan Kesihatan": {
        "namaEkonomi": ["", "Kemudahan Kesihatan"]
    },
    "Kemudahan Pendidikan": {
        "namaEkonomi": ["", "Institusi Pendidikan", "Perpustakaan Desa", "Pusat ICT"]
    },
    "Kemudahan Pengangkutan": {
        "namaEkonomi": ["", "Kemudahan Pengangkutan Awam"]
    },
    "Kemudahan Pentadbiran Masyarakat": {
        "namaEkonomi": ["", "Kemudahan Pentadbiran & Masyarakat"]
    },
    "Kemudahan Rekreasi": {
        "namaEkonomi": ["", "Kemudahan Rekreasi", "Kemudahan Sukan", "Tanah Lapang"]
    }
}

const detailKemaskiniNamaKemudahan = {
    "Bengkel/Kilang": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BengkelKilang_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Chalet/Homestay": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/ChaletHomestay_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Perkhidmatan Bank": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PerkhidmatanBank_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Pusat Penjaja": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PusatPenjaja_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Perkuburan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Perkuburan_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Rumah Ibadat": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/RumahIbadat_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Bomba": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BalaiBomba_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Polis": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BalaiPolis_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Tapak Kubu Monumen": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/TapakKubuMonumen_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Kemudahan Kesihatan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Kesihatan_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Institusi Pendidikan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/InstitusiPendidikan_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Perpustakaan Desa": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PerpustakaanDesa_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Pusat ICT": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PusatICT_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Kemudahan Pengangkutan Awam": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PengangkutanAwam_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Kemudahan Pentadbiran & Masyarakat": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PentadbiranMasyarakat_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Kemudahan Rekreasi": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Rekreasi_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Kemudahan Sukan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Sukan_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    },
    "Tanah Lapang": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/TanahLapang_Editor/FeatureServer",
        "fieldConfig": [kemaskiniNamaBaruKemudahan, kemaskiniCatatan]
    }
};

const detailNamaKemudahan = {
    "Bengkel/Kilang": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BengkelKilang_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Chalet/Homestay": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/ChaletHomestay_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Perkhidmatan Bank": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PerkhidmatanBank_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Pusat Penjaja": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PusatPenjaja_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Perkuburan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Perkuburan_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Rumah Ibadat": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/RumahIbadat_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Bomba": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BalaiBomba_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Polis": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/BalaiPolis_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Tapak Kubu Monumen": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/TapakKubuMonumen_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Kemudahan Kesihatan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Kesihatan_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Institusi Pendidikan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/InstitusiPendidikan_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Perpustakaan Desa": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PerpustakaanDesa_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Pusat ICT": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PusatICT_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Kemudahan Pengangkutan Awam": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PengangkutanAwam_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Kemudahan Pentadbiran & Masyarakat": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/PentadbiranMasyarakat_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Kemudahan Rekreasi": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Rekreasi_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Kemudahan Sukan": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/Sukan_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    },
    "Tanah Lapang": {
        "url": "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/TanahLapang_Editor/FeatureServer",
        "fieldConfig": [inputNamaBaruKemudahan, inputCatatan]
    }
};

// Start Page Function
var slideIndex = 1;

function configureFramework() {
    $.get("framework.html", function (html) {
        document.getElementById("id_framework").innerHTML = html;
        if (window.location.pathname == "/sumber.info.html") {
            document.getElementById("id_topbar_title").innerHTML = "Sumber: Info";

            showDivs(slideIndex);
        } else if (window.location.pathname == "/sumber.tambah.html") {
            document.getElementById("id_topbar_title").innerHTML = "Sumber: Tambah Data";
            loadMap();
            document.getElementById("pilih_jenisKemudahan_sor_tambahan").addEventListener('change', changeJenisKemudahanSort);
            document.getElementById("pilih_namaKemudahan_sor_tambahan").addEventListener('change', changeNamaKemudahanSort);
        } else if (window.location.pathname == "/sumber.kemaskini.html") {
            document.getElementById("id_topbar_title").innerHTML = "Sumber: Kemaskini Data";
            loadMap();
            document.getElementById("pilih_jenisKemudahan_sor_kemaskini").addEventListener('change', changeJenisKemudahanSork);
            document.getElementById("pilih_namaKemudahan_sor_kemaskini").addEventListener('change', changeNamaKemudahanSork);
        } else {
            document.getElementById("id_topbar_title").innerHTML = "Sumber Orang Ramai";
        }

    });
}

function goToInfo() {
    window.location.href = 'sumber.info.html';
}

function goToAddData() {
    window.location.href = 'sumber.tambah.html';
}

function goToUpdateData() {
    window.location.href = 'sumber.kemaskini.html';
}

function goToScharms() {
    window.location.href = 'mengenai_scharms.html';
}

function goToPerkhidmatan() {
    window.location.href = 'perkhidmatan.html';
}

function goToUtama() {
    window.location.href = 'main.html';
}

function goToHubungi() {
    window.location.href = 'hubungi_kami.html';
}

function goToProfil() {
    window.location.href = 'profil.html';
}

//Slider 
function plusDivs(n) {
    showDivs(slideIndex += n);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot-indicator");
    if (n > x.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = x.length
    }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-white", "");
    }
    x[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " w3-white";
}
// End Slider

function radiobtnkampung(obj) {
    // let radiobtn = document.getElementsByName("jeniskampung");
    // for (let i = 0; i < radiobtn.length; i++) {
    //     console.log(radiobtn[i].checked);
    // }
    let radiovalue = obj.value;
    if(radiovalue == "kemudahan_kampung"){
        document.getElementById("divSempadanKg").style.display ="none";
        document.getElementById("divKemudahanKg").style.display ="block";
    }else if(radiovalue == "sempadan_kampung"){
        document.getElementById("divSempadanKg").style.display ="block";
        document.getElementById("divKemudahanKg").style.display ="none";
    }
}

function checkTitle(obj, titletext) {
    let titlename = document.getElementById("id_topbar_title").innerHTML;
    if (titlename != titletext) {
        document.getElementById("id_topbar_title").innerHTML = titletext;
        document.getElementById("sumber_awam_section").style.display = "block";
        obj.classList.add('w3-green');
    } else {
        document.getElementById("id_topbar_title").innerHTML = "Sumber Orang Ramai";
        document.getElementById("sumber_awam_section").style.display = "none";
        obj.classList.remove('w3-green');
    }
}

function selectedBar() {
    let sumber_bar_btn = document.querySelectorAll('.sumber-bar-btn');
    for (let i = 0; i < sumber_bar_btn.length; i++) {
        sumber_bar_btn[i].classList.remove("w3-green");
    }
}
// End Page Function
// Start Arcgis
function loadMap() {
    require([
        "esri/Map",
        "esri/views/MapView",
        "esri/layers/MapImageLayer",
        "esri/tasks/QueryTask",
        "esri/tasks/support/Query",
        "esri/widgets/CoordinateConversion",
        "esri/widgets/Locate",
        "esri/widgets/Home",
        "esri/widgets/Compass",
        "esri/widgets/Track",
    ], (
        Map,
        MapView,
        MapImageLayer,
        QueryTask, Query,
        CoordinateConversion,
        Locate,
        Home,
        Compass,
        Track,

    ) => {
        map = new Map({
            basemap: "streets",
            ground: "world-elevation"
        });

        let demarcation = new MapImageLayer({
            url: "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/Demarcation/MapServer"
        });

        let kgrangkaian_editor = new MapImageLayer({
            url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungRangkaian_Editor/MapServer"
        });
        let kginduk_editor = new MapImageLayer({
            url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungInduk/MapServer"
        });

        map.addMany([
            demarcation,
            kgrangkaian_editor,
            kginduk_editor
        ]);
        view = new MapView({
            container: "viewDiv",
            map: map,
            extent: {
                "xmin": 99,
                "ymin": 1,
                "xmax": 120,
                "ymax": 7,
                "spatialReference": 4326
            }
        });
        var ccWidget = new CoordinateConversion({
            view: view
        });
        var locateBtn = new Locate({
            view: view
        });
        var homeBtn = new Home({
            view: view
        });
        var compassWidget = new Compass({
            view: view
          });
        var track = new Track({
            view: view
        });
        view.ui.add(ccWidget, "bottom-left");
        view.ui.add(locateBtn, {
            position: "top-left"
          });
        view.ui.add(homeBtn, "top-left");
        view.ui.add(compassWidget, "top-left");
        view.ui.add(track, "top-left");
        
        queryNegeriSort(true);
        queryDaerahSort("1=1", false);
        queryMukimSort("1=1", false);
        queryKampungSort("1=1", false);

        function queryNegeriSort(zoomto = true) {
            document.getElementById("negeriselection").innerHTML = '<option value="" disabled selected>Negeri</option>';
            document.getElementById("negeriselectionX").innerHTML = '<option value="" disabled selected>Negeri</option>';
            let queryTask = new QueryTask({
                url: urlnegeri
            });
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.order = ["nama_neger asc"];
            query.where = "1=1";

            queryTask.execute(query).then(function (results) {
                let features = results.features;

                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_neger"];
                    option.value = features[i].attributes["kod_negeri"];
                    document.getElementById("negeriselection").options.add(option);
                }

                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_neger"];
                    option.value = features[i].attributes["kod_negeri"];
                    document.getElementById("negeriselectionX").options.add(option);
                }
                if (zoomto)
                    view.goTo(features);
            });
            document.getElementById("daerahselection").innerHTML = '<option value="" disabled selected>Daerah</option>';
            document.getElementById("mukimselection").innerHTML = '<option value="" disabled selected>Mukim</option>';
            document.getElementById("kampungselection").innerHTML = '<option value="" disabled selected>Kampung</option>';

            document.getElementById("daerahselectionX").innerHTML = '<option value="" disabled selected>Daerah</option>';
            document.getElementById("mukimselectionX").innerHTML = '<option value="" disabled selected>Mukim</option>';
            // document.getElementById("kampungselectionX").innerHTML = '<option value="" disabled selected>Kampung</option>';
        }

        function queryDaerahSort(obj, zoomto = true) {
            // console.log(obj);
            // console.log(this);
            // console.log(this.value);
            let paramq = "";
            if (obj == "1=1") {
                paramq = "1=1";
            } else {
                paramq = "kod_negeri = '" + this.value + "'";
            }
            // console.log(paramq)
            document.getElementById("daerahselection").innerHTML = '<option value="" disabled selected>Daerah</option>';
            document.getElementById("daerahselectionX").innerHTML = '<option value="" disabled selected>Daerah</option>';
            let queryTask = new QueryTask({
                url: urldaerah
            });
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.order = ["nama_daera asc"];
            query.where = paramq;

            queryTask.execute(query).then(function (results) {
                // console.log(results.features);
                let features = results.features;

                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_daera"];
                    option.value = features[i].attributes["kod_daerah"];
                    document.getElementById("daerahselection").options.add(option);
                }

                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_daera"];
                    option.value = features[i].attributes["kod_daerah"];
                    document.getElementById("daerahselectionX").options.add(option);
                }
                if (zoomto)
                    view.goTo(features);
            });
            document.getElementById("mukimselection").innerHTML = '<option value="" disabled selected>Mukim</option>';
            document.getElementById("kampungselection").innerHTML = '<option value="" disabled selected>Kampung</option>';

            document.getElementById("mukimselectionX").innerHTML = '<option value="" disabled selected>Mukim</option>';
            // document.getElementById("kampungselectionX").innerHTML = '<option value="" disabled selected>Kampung</option>';
        }

        function queryMukimSort(obj, zoomto = true) {
            // console.log(obj);
            // console.log(this);
            // console.log(this.value);
            let paramq = "";
            if (obj == "1=1") {
                paramq = "1=1";
            } else {
                paramq = "kod_daerah = '" + this.value + "'";
            }
            // console.log(paramq)
            document.getElementById("mukimselection").innerHTML = '<option value="" disabled selected>Mukim</option>';
            document.getElementById("mukimselectionX").innerHTML = '<option value="" disabled selected>Mukim</option>';
            let queryTask = new QueryTask({
                url: urlmukim
            });
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.order = ["nama_mukim asc"];
            query.where = paramq;

            queryTask.execute(query).then(function (results) {
                // console.log(results.features)
                let features = results.features;

                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_mukim"];
                    option.value = features[i].attributes["kod_mukim"];
                    document.getElementById("mukimselection").options.add(option);
                }
                for (let i = 0; i < features.length; i++) {
                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_mukim"];
                    option.value = features[i].attributes["kod_mukim"];
                    document.getElementById("mukimselectionX").options.add(option);
                }
                if (zoomto)
                    view.goTo(features);

            });
            document.getElementById("kampungselection").innerHTML = '<option value="" disabled selected>Kampung</option>';
        }

        function queryKampungSort(obj, zoomto = true) {
            // console.log(obj);
            // console.log(this);
            // console.log(this.value);
            let paramq = "";
            if (obj == "1=1") {
                paramq = "1=1";
            } else {
                paramq = "mukim = '" + this.value + "'";
            }
            // console.log(paramq)
            document.getElementById("kampungselection").innerHTML = '<option value="" disabled selected>Kampung</option>';
            let queryTask = new QueryTask({
                url: urlkampung
            });
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.order = ["nama_kg asc"];
            query.where = paramq;

            queryTask.execute(query).then(function (results) {
                // console.log(results.features);
                let features = results.features;

                for (let i = 0; i < features.length; i++) {

                    var option = document.createElement("option");
                    option.text = features[i].attributes["nama_kg"];
                    option.value = features[i].attributes["id_kg"];
                    document.getElementById("kampungselection").options.add(option);
                }
                if (zoomto)
                    view.goTo(features);

            });

        }

        function zoomToKampung(obj, zoomto = true) {
            let paramq = "";
            if (obj == "1=1") {
                paramq = "1=1";
            } else {
                paramq = "id_kg = '" + this.value + "'";
            }
            // console.log(paramq)

            let queryTask = new QueryTask({
                url: urlkampung
            });
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.order = ["nama_kg asc"];
            query.where = paramq;

            queryTask.execute(query).then(function (results) {
                // console.log(results.features);
                let features = results.features;

                if (zoomto)
                    view.goTo(features);

            });
        }
        document.getElementById("negeriselection").addEventListener('change', queryDaerahSort);
        document.getElementById("daerahselection").addEventListener('change', queryMukimSort);
        document.getElementById("mukimselection").addEventListener('change', queryKampungSort);
        document.getElementById("kampungselection").addEventListener('change', zoomToKampung);

        document.getElementById("negeriselectionX").addEventListener('change', queryDaerahSort);
        document.getElementById("daerahselectionX").addEventListener('change', queryMukimSort);
        document.getElementById("mukimselectionX").addEventListener('change', queryKampungSort);
        // document.getElementById("kampungselection").addEventListener('change', zoomToKampung);




    });
}
// Code nuzul
function changeJenisKemudahanSort() {
    var valueOption = this.value;
    // console.log(valueOption);

    var selectNamaKemudahan = document.getElementById("pilih_namaKemudahan_sor_tambahan")
    selectNamaKemudahan.innerHTML = null;
    // console.log(arrayJenisKemudahan);

    queryNamaKemudahanSort(valueOption);

}

function queryNamaKemudahanSort(jenisKemudahan) {
    var selectNamaKemudahan = document.getElementById("pilih_namaKemudahan_sor_tambahan")
    if (jenisKemudahan == "none") {
        selectNamaKemudahan.innerHTML = "<option></option>";
    } else if (jenisKemudahan == "Kemudahan Ekonomi") {
        var k_Ekonomi = arrayJenisKemudahan["Kemudahan Ekonomi"].namaEkonomi;
        for (let i = 0; i < k_Ekonomi.length; i++) {
            // console.log(k_Ekonomi[i]);
            var option = document.createElement("option");
            option.text = k_Ekonomi[i];
            option.value = k_Ekonomi[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Keagamaan") {
        var k_Keagamaan = arrayJenisKemudahan["Kemudahan Keagamaan"].namaEkonomi;
        for (let i = 0; i < k_Keagamaan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Keagamaan[i];
            option.value = k_Keagamaan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Keselamatan") {
        var k_Keselamatan = arrayJenisKemudahan["Kemudahan Keselamatan"].namaEkonomi;
        for (let i = 0; i < k_Keselamatan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Keselamatan[i];
            option.value = k_Keselamatan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Kesihatan") {
        var k_Kesihatan = arrayJenisKemudahan["Kemudahan Kesihatan"].namaEkonomi;
        for (let i = 0; i < k_Kesihatan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Kesihatan[i];
            option.value = k_Kesihatan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pendidikan") {
        var k_Pendidikan = arrayJenisKemudahan["Kemudahan Pendidikan"].namaEkonomi;
        for (let i = 0; i < k_Pendidikan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Pendidikan[i];
            option.value = k_Pendidikan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pengangkutan") {
        var k_Pengangkutan = arrayJenisKemudahan["Kemudahan Pengangkutan"].namaEkonomi;
        for (let i = 0; i < k_Pengangkutan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Pengangkutan[i];
            option.value = k_Pengangkutan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pentadbiran Masyarakat") {
        var k_PentadbiranMasyarakat = arrayJenisKemudahan["Kemudahan Pentadbiran Masyarakat"].namaEkonomi;
        for (let i = 0; i < k_PentadbiranMasyarakat.length; i++) {
            var option = document.createElement("option");
            option.text = k_PentadbiranMasyarakat[i];
            option.value = k_PentadbiranMasyarakat[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Rekreasi") {
        var k_Rekreasi = arrayJenisKemudahan["Kemudahan Rekreasi"].namaEkonomi;
        for (let i = 0; i < k_Rekreasi.length; i++) {
            var option = document.createElement("option");
            option.text = k_Rekreasi[i];
            option.value = k_Rekreasi[i];
            selectNamaKemudahan.options.add(option);
        }
    }
}

function changeNamaKemudahanSort() {

    require([
        "esri/layers/FeatureLayer",
        "esri/widgets/Editor",
        "esri/widgets/Expand"
    ], (
        FeatureLayer,
        Editor,
        Expand
    ) => {
        var namaKemudahan = this.value;
        // console.log(namaKemudahan);

        if (!(typeof detailNamaKemudahan[namaKemudahan] === 'undefined')) {

            // Remove First if layer already add in map
            map.removeMany([
                layerKgInduk_sort,
                layerKgRangkaian_sort,
                layerEdit_sort
            ]);
            view.ui.remove(editorExpand_sort);

            layerKgInduk_sort = new FeatureLayer({
                // URL to the service
                url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungInduk_Editor/MapServer"
            });

            layerKgRangkaian_sort = new FeatureLayer({
                // URL to the service
                url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungRangkaian_Editor/MapServer"
            });

            layerEdit_sort = new FeatureLayer({
                // URL to the service
                url: detailNamaKemudahan[namaKemudahan]["url"]
            });
            map.addMany([
                layerKgInduk_sort,
                layerKgRangkaian_sort,
                layerEdit_sort
            ]);

            view.when(() => {
                const editor = new Editor({
                    view: view,
                    allowedWorkflows: ["create"], //create/update
                    layerInfos: [{
                        layer: layerEdit_sort,
                        fieldConfig: detailNamaKemudahan[namaKemudahan]["fieldConfig"]
                    }]
                });

                // let attch = new Attachments()

                // Add widget to top-right of the view
                editorExpand_sort = new Expand({
                    expandIconClass: "esri-icon-edit", // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
                    expandTooltip: "Editing Tools", // optional, defaults to "Expand" for English locale
                    view: view,
                    content: editor
                });

                view.ui.add(editorExpand_sort, "top-right");

            });
        }
    });
}


function changeJenisKemudahanSork() {
    var valueOption = this.value;

    var selectNamaKemudahan = document.getElementById("pilih_namaKemudahan_sor_kemaskini")
    selectNamaKemudahan.innerHTML = null;

    queryNamaKemudahanSork(valueOption);

}

function changeNamaKemudahanSork() {
    require([
        "esri/layers/FeatureLayer",
        "esri/widgets/Editor",
        "esri/widgets/Expand"
    ], (
        FeatureLayer,
        Editor,
        Expand
    ) => {
        var namaKemudahan = this.value;
        // console.log(namaKemudahan);


        if (!(typeof detailKemaskiniNamaKemudahan[namaKemudahan] === 'undefined')) {

            // Remove First if layer already add in map
            map.removeMany([
                layerKgInduk_sork,
                layerKgRangkaian_sork,
                layerEdit_sork
            ]);
            view.ui.remove(editorExpand_sork);

            layerKgInduk_sork = new FeatureLayer({
                // URL to the service
                url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungInduk_Editor/MapServer"
            });

            layerKgRangkaian_sork = new FeatureLayer({
                // URL to the service
                url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungRangkaian_Editor/MapServer"
            });

            layerEdit_sork = new FeatureLayer({
                // URL to the service
                url: detailKemaskiniNamaKemudahan[namaKemudahan]["url"]
            });

            map.addMany([
                layerKgInduk_sork,
                layerKgRangkaian_sork,
                layerEdit_sork
            ]);


            view.when(() => {
                const editor = new Editor({
                    view: view,
                    allowedWorkflows: ["update"], //create/update
                    layerInfos: [{
                        layer: layerEdit_sork
                        // fieldConfig: detailKemaskiniNamaKemudahan[namaKemudahan]["fieldConfig"]
                    }]
                });

                // let attch = new Attachments()

                // Add widget to top-right of the view
                editorExpand_sork = new Expand({
                    expandIconClass: "esri-icon-edit", // see https://developers.arcgis.com/javascript/latest/guide/esri-icon-font/
                    expandTooltip: "Editing Tools", // optional, defaults to "Expand" for English locale
                    view: view,
                    content: editor
                });

                view.ui.add(editorExpand_sork, "top-right");

            });



        }
    });
}

function queryNamaKemudahanSork(jenisKemudahan) {
    var selectNamaKemudahan = document.getElementById("pilih_namaKemudahan_sor_kemaskini")
    if (jenisKemudahan == "none") {
        selectNamaKemudahan.innerHTML = "<option></option>";
    } else if (jenisKemudahan == "Kemudahan Ekonomi") {
        var k_Ekonomi = arrayJenisKemudahan_Kemaskini["Kemudahan Ekonomi"].namaEkonomi;
        for (let i = 0; i < k_Ekonomi.length; i++) {
            // console.log(k_Ekonomi[i]);
            var option = document.createElement("option");
            option.text = k_Ekonomi[i];
            option.value = k_Ekonomi[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Keagamaan") {
        var k_Keagamaan = arrayJenisKemudahan_Kemaskini["Kemudahan Keagamaan"].namaEkonomi;
        for (let i = 0; i < k_Keagamaan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Keagamaan[i];
            option.value = k_Keagamaan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Keselamatan") {
        var k_Keselamatan = arrayJenisKemudahan_Kemaskini["Kemudahan Keselamatan"].namaEkonomi;
        for (let i = 0; i < k_Keselamatan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Keselamatan[i];
            option.value = k_Keselamatan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Kesihatan") {
        var k_Kesihatan = arrayJenisKemudahan_Kemaskini["Kemudahan Kesihatan"].namaEkonomi;
        for (let i = 0; i < k_Kesihatan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Kesihatan[i];
            option.value = k_Kesihatan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pendidikan") {
        var k_Pendidikan = arrayJenisKemudahan_Kemaskini["Kemudahan Pendidikan"].namaEkonomi;
        for (let i = 0; i < k_Pendidikan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Pendidikan[i];
            option.value = k_Pendidikan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pengangkutan") {
        var k_Pengangkutan = arrayJenisKemudahan_Kemaskini["Kemudahan Pengangkutan"].namaEkonomi;
        for (let i = 0; i < k_Pengangkutan.length; i++) {
            var option = document.createElement("option");
            option.text = k_Pengangkutan[i];
            option.value = k_Pengangkutan[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Pentadbiran Masyarakat") {
        var k_PentadbiranMasyarakat = arrayJenisKemudahan_Kemaskini["Kemudahan Pentadbiran Masyarakat"].namaEkonomi;
        for (let i = 0; i < k_PentadbiranMasyarakat.length; i++) {
            var option = document.createElement("option");
            option.text = k_PentadbiranMasyarakat[i];
            option.value = k_PentadbiranMasyarakat[i];
            selectNamaKemudahan.options.add(option);
        }
    } else if (jenisKemudahan == "Kemudahan Rekreasi") {
        var k_Rekreasi = arrayJenisKemudahan_Kemaskini["Kemudahan Rekreasi"].namaEkonomi;
        for (let i = 0; i < k_Rekreasi.length; i++) {
            var option = document.createElement("option");
            option.text = k_Rekreasi[i];
            option.value = k_Rekreasi[i];
            selectNamaKemudahan.options.add(option);
        }
    }
}