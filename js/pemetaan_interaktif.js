$.get("framework.html", function (html) {
    document.getElementById("id_framework").innerHTML = html;
    document.getElementById("id_topbar_title").innerHTML = 'Pemetaan Interaktif';

});

function openTab(evt, labelName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("tab-label");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
    }
    document.getElementById(labelName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-red";
}

var map, view;
require([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/MapImageLayer",
    "esri/tasks/QueryTask",
    "esri/tasks/support/Query",
    "esri/widgets/CoordinateConversion",
    "esri/widgets/Locate",
    "esri/widgets/Home",
    "esri/widgets/Compass",
    "esri/widgets/Track",
    "esri/widgets/LayerList",
    "esri/widgets/BasemapToggle",
    "esri/widgets/BasemapGallery"
], (
    Map,
    MapView,
    MapImageLayer,
    QueryTask, Query, // guna tuk xy kalu x coodinateconversion xjln
    CoordinateConversion,
    Locate,
    Home,
    Compass,
    Track,
    LayerList,
    BasemapToggle,
    BasemapGallery,


) => {
    map = new Map({
        basemap: "streets",
        ground: "world-elevation"
    });

    let demarcation = new MapImageLayer({
        url: "https://dev.serasi.tech/arcgis/rest/services/SCHARMS/Demarcation/MapServer"
    });

    let kgrangkaian_editor = new MapImageLayer({
        url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungRangkaian_Editor/MapServer"
    });
    let kginduk_editor = new MapImageLayer({
        url: "https://dev.serasi.tech/arcgis/rest/services/PDDM_EDIT/KampungInduk/MapServer"
    });

    map.addMany([
        demarcation,
        kgrangkaian_editor,
        kginduk_editor
    ]);
    view = new MapView({
        container: "viewDiv",
        map: map,
        extent: {
            "xmin": 99,
            "ymin": 1,
            "xmax": 120,
            "ymax": 7,
            "spatialReference": 4326
        }
    });
    var ccWidget = new CoordinateConversion({
        view: view
    });
    var locateBtn = new Locate({
        view: view
    });
    var homeBtn = new Home({
        view: view
    });
    var compassWidget = new Compass({
        view: view
    });
    var track = new Track({
        view: view
    });
    var layerList = new LayerList({
        view: view
    });
    var basemapGallery = new BasemapGallery({
        view: view
    });
    var basemapToggle = new BasemapToggle({
        view: view,
        // nextBasemap: "arcgis-imagery"
     });
    view.ui.add(ccWidget, "bottom-right");
    view.ui.add(locateBtn, {
        position: "top-left"
    });
    view.ui.add(homeBtn, "top-left");
    view.ui.add(compassWidget, "top-left");
    view.ui.add(track, "top-left");
    view.when(function () {
        // Add widget to the top right corner of the view
        view.ui.add(layerList, "top-right");
    });
    view.ui.add(basemapGallery, {
        position: "top-right"
    });
    view.ui.add(basemapToggle,"bottom-left");

});